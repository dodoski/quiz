package com.company;

class QuizImpl implements Quiz{

    private int digit;

    public QuizImpl() {
        this.digit = 254;	// zmienna moze ulegac zmianie!
    }

    // implementacja metody isCorrectValue...
    @Override
    public void isCorrectValue(int value) throws Quiz.ParamTooLarge, Quiz.ParamTooSmall {
        if(value > digit) {
            throw new Quiz.ParamTooLarge();
        }
        else if(value < digit){
            throw new Quiz.ParamTooSmall();
        }
    }
}
